package org.bitbucket.infovillavendor.blooddonor.cells;

import javafx.scene.control.ListCell;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodType;

/**
 * Created by Thant Zin Oo on 10/17/2014.
 */
public class BloodTypeCell extends ListCell<BloodType> {
    @Override
    protected void updateItem(BloodType item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null)
            setText(item.getBloodType());
    }
}
