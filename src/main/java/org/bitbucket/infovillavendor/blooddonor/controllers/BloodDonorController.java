package org.bitbucket.infovillavendor.blooddonor.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.bitbucket.infovillavendor.blooddonor.cells.BloodTypeCell;
import org.bitbucket.infovillavendor.blooddonor.dao.BloodDonorDao;
import org.bitbucket.infovillavendor.blooddonor.dao.BloodTypeDao;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodDonor;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodType;
import org.bitbucket.infovillavendor.blooddonor.entities.enums.Gender;
import org.bitbucket.infovillavendor.blooddonor.entitywrappers.BloodDonorWrapper;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Thant Zin Oo on 10/17/2014.
 */
@FXMLController(value = "/fxml/blood_donor.fxml", title = "Blood Donor")
public class BloodDonorController {
    @FXML
    @ActionTrigger("deleteDonor")
    Button delete;
    @FXML
    @ActionTrigger("editDonor")
    Button edit;
    @FXML
    @ActionTrigger("editLastDonationDate")
    Button editLastDonationDate;
    @FXML
    @ActionTrigger("loadDonorAddForm")
    Button createNewDonor;
    @FXML
    TableView<BloodDonorWrapper> bloodDonorListTable;
    @FXML
    TableColumn<BloodDonorWrapper, String> donorName;
    @FXML
    TableColumn<BloodDonorWrapper, String> donorMyanmarName;
    @FXML
    TableColumn<BloodDonorWrapper, Integer> donorAge;
    @FXML
    TableColumn<BloodDonorWrapper, Gender> donorGender;
    @FXML
    TableColumn<BloodDonorWrapper, BloodType> donorBloodType;
    @FXML
    GridPane addGrid;
    @FXML
    TextField name;
    @FXML
    TextField myanmarName;
    @FXML
    TextField age;
    @FXML
    ToggleGroup gender;
    @FXML
    RadioButton male;
    @FXML
    RadioButton female;
    @FXML
    ComboBox<BloodType> bloodTypeComboBox;
    @FXML
    TextField phone;
    @FXML
    TextField email;
    @FXML
    TextField facebook;
    @FXML
    GridPane showGrid;
    @FXML
    GridPane editLastDonationGrid;
    @FXML
    Text showName;
    @FXML
    Text showMyanmarName;
    @FXML
    Text showAge;
    @FXML
    Text showGender;
    @FXML
    Text showBloodType;
    @FXML
    Text showPhone;
    @FXML
    Text showEmail;
    @FXML
    Text showFacebook;
    @FXML
    @ActionTrigger("saveDonor")
    Button save;
    @FXML
    @ActionTrigger("addBloodDonor")
    Button create;
    @FXML
    TextField searchName;
    @FXML
    ComboBox<String> searchBloodType;
    private boolean isMale;
    private BloodDonorWrapper currentBloodDonor;

    @PostConstruct
    public void init() {
        selectMale();
        setupRowClickListener();
        loadTable();
        loadNewDonor();
        setupSearchListeners();
    }

    private void setupSearchListeners() {
        searchName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String bloodTypeInSearch = searchBloodType.getValue();
                if (bloodTypeInSearch.equals("All"))
                    bloodTypeInSearch = null;
                loadTableBySearchTerms(newValue, bloodTypeInSearch);
            }
        });

        searchBloodType.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String bloodTypeInSearch = newValue;
                if (bloodTypeInSearch.equals("All"))
                    bloodTypeInSearch = null;
                loadTableBySearchTerms(searchName.getText(), bloodTypeInSearch);
            }
        });
    }

    private void setupRowClickListener() {
        bloodDonorListTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<BloodDonorWrapper>() {
            @Override
            public void changed(ObservableValue<? extends BloodDonorWrapper> observable, BloodDonorWrapper oldValue, BloodDonorWrapper newValue) {
                currentBloodDonor = newValue;

                showGrid.setVisible(true);
                addGrid.setVisible(false);
                editLastDonationGrid.setVisible(false);
                showGrid.toFront();
                if (newValue != null) {
                    showName.setText(newValue.getName());
                    showMyanmarName.setText(newValue.getMyanmarName());
                    showAge.setText(newValue.getAge() + "");
                    showGender.setText(newValue.getGender());
                    showBloodType.setText(newValue.getBloodType());
                    showPhone.setText(newValue.getPhone());
                    showEmail.setText(newValue.getEmail());
                    showFacebook.setText(newValue.getFacebook());
                }
            }
        });
    }

    private void loadTable() {

        List<BloodDonor> bloodDonors = BloodDonorDao.getBloodDonors();
        List<BloodDonorWrapper> bloodDonorWrappers = new ArrayList<BloodDonorWrapper>();
        for (BloodDonor bloodDonor : bloodDonors) {
            bloodDonorWrappers.add(new BloodDonorWrapper(bloodDonor));
        }
        bloodDonorListTable.getItems().setAll(bloodDonorWrappers);
    }

    private void loadTableBySearchTerms(String name, String bloodType) {

        List<BloodDonor> bloodDonors = BloodDonorDao.findBloodDonorsByNameLikeAndBloodType(name, bloodType);
        List<BloodDonorWrapper> bloodDonorWrappers = new ArrayList<BloodDonorWrapper>();
        for (BloodDonor bloodDonor : bloodDonors) {
            bloodDonorWrappers.add(new BloodDonorWrapper(bloodDonor));
        }
        bloodDonorListTable.getItems().setAll(bloodDonorWrappers);
    }

    private void clearFields() {
        name.clear();
        myanmarName.clear();
        age.clear();
        phone.clear();
        email.clear();
        facebook.clear();
        bloodTypeComboBox.getSelectionModel().clearSelection();
        selectMale();
    }

    private void selectMale() {
        gender.selectToggle(male);
        isMale = true;
    }

    private void loadNewDonor() {
        List<BloodType> bloodTypes = BloodTypeDao.getBloodTypes();
        bloodTypeComboBox.getItems().setAll(bloodTypes);
        bloodTypeComboBox.setValue(bloodTypeComboBox.getItems().get(0));

        save.setVisible(false);
        create.setVisible(true);
        create.toFront();

        bloodTypeComboBox.setButtonCell(new BloodTypeCell());
        bloodTypeComboBox.setCellFactory(new Callback<ListView<BloodType>, ListCell<BloodType>>() {
                                             @Override
                                             public ListCell<BloodType> call(ListView<BloodType> param) {
                                                 return new BloodTypeCell();
                                             }
                                         }

        );

        gender.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (gender.getSelectedToggle() == male)
                    isMale = true;
            }
        });
    }

    private void refreshTable() {
        bloodDonorListTable.getItems().clear();
        loadTable();
    }

    private void loadDonorForm() {
        showGrid.setVisible(false);
        editLastDonationGrid.setVisible(false);
        addGrid.setVisible(true);
        addGrid.toFront();
        clearFields();
    }

    @ActionMethod("addBloodDonorMethod")
    public void addBloodDonorMethod() {
        BloodDonor bloodDonor = new BloodDonor();
        bloodDonor.setName(name.getText());
        bloodDonor.setMyanmarName(myanmarName.getText());
        bloodDonor.setBloodType(bloodTypeComboBox.getValue());
        bloodDonor.setAge(Integer.parseInt(age.getText()));
        bloodDonor.setGender(isMale ? Gender.MALE : Gender.FEMALE);
        bloodDonor.setPhone(phone.getText());
        bloodDonor.setEmail(email.getText());
        bloodDonor.setFacebook(facebook.getText());
        bloodDonor.setCreateDate(new Date());
        BloodDonorDao.saveBloodDonor(bloodDonor);
        refreshTable();
        clearFields();
    }

    @ActionMethod("saveDonor")
    public void saveDonor() {
        BloodDonor bloodDonor = currentBloodDonor.getBloodDonor();
        bloodDonor.setName(name.getText());
        bloodDonor.setMyanmarName(myanmarName.getText());
        bloodDonor.setBloodType(bloodTypeComboBox.getValue());
        bloodDonor.setAge(Integer.parseInt(age.getText()));
        bloodDonor.setGender(isMale ? Gender.MALE : Gender.FEMALE);
        bloodDonor.setPhone(phone.getText());
        bloodDonor.setEmail(email.getText());
        bloodDonor.setFacebook(facebook.getText());
        bloodDonor.setCreateDate(new Date());
        BloodDonorDao.updateBloodDonor(bloodDonor);
        refreshTable();
        clearFields();
    }

    @ActionMethod("loadDonorAddForm")
    public void loadDonorAddForm() {
        save.setVisible(false);
        create.setVisible(true);
        create.toFront();
        loadDonorForm();
        bloodTypeComboBox.setValue(bloodTypeComboBox.getItems().get(0));
    }

    @ActionMethod("deleteDonor")
    public void deleteDonor() {
        BloodDonorDao.deleteBloodDonor(currentBloodDonor.getBloodDonor());
        currentBloodDonor = null;
        refreshTable();
    }
    @ActionMethod("editDonor")
    public void editDonor(){
        loadDonorForm();
        create.setVisible(false);
        save.setVisible(true);
        save.toFront();

        if (currentBloodDonor !=  null) {
            name.setText(currentBloodDonor.getBloodDonor().getName());
            myanmarName.setText(currentBloodDonor.getBloodDonor().getMyanmarName());
            age.setText(currentBloodDonor.getAge());
            gender.selectToggle(currentBloodDonor.getBloodDonor().getGender() == Gender.MALE ? male : female);
            List<BloodType> bloodTypesInSelection = bloodTypeComboBox.getItems();
            BloodType selectedBloodType = null;
            for (BloodType bloodType : bloodTypesInSelection) {
                if (bloodType.getId() == currentBloodDonor.getBloodDonor().getBloodType().getId())
                    selectedBloodType = bloodType;
            }
            bloodTypeComboBox.setValue(selectedBloodType);
            phone.setText(currentBloodDonor.getBloodDonor().getPhone());
            email.setText(currentBloodDonor.getBloodDonor().getEmail());
            facebook.setText(currentBloodDonor.getBloodDonor().getFacebook());

        }
    }
    @ActionMethod("editLastDonationDate")
    public void editLastDonationDate(){
        showGrid.setVisible(false);
        addGrid.setVisible(false);
        editLastDonationGrid.setVisible(true);
        editLastDonationGrid.toFront();
    }
}
