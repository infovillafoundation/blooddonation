package org.bitbucket.infovillavendor.blooddonor.dao;

import org.bitbucket.infovillavendor.blooddonor.entities.BloodDonor;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodType;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Thant Zin Oo on 10/17/2014.
 */
public class BloodDonorDao extends Dao {

    public static List<BloodDonor> findBloodDonorsByNameLikeAndBloodType(String name, String bloodType) {
        EntityManager em = emf.createEntityManager();
        String queryString = "BloodDonor.findByNameLikeAndBloodType";
        if (bloodType == null)
            queryString = "BloodDonor.findByNameLike";
        TypedQuery<BloodDonor> query = em.createNamedQuery(queryString, BloodDonor.class);
        if(name!=null)
          query.setParameter("name", "%" + name + "%");
        if(bloodType!=null)
            query.setParameter("bloodType", bloodType);
        List<BloodDonor> bloodDonors = query.getResultList();
        return bloodDonors;
    }

    public static void updateBloodDonor(BloodDonor bloodDonor) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(bloodDonor);
        em.getTransaction().commit();
        em.close();
    }

    public static void saveBloodDonor(BloodDonor bloodDonor) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(bloodDonor);
        em.getTransaction().commit();
        em.close();
    }

    public static List<BloodDonor> getBloodDonors() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<BloodDonor> query = em.createNamedQuery("BloodDonor.findAll", BloodDonor.class);
        List<BloodDonor> bloodDonors = query.getResultList();
        return bloodDonors;
    }

    public static void deleteBloodDonor(BloodDonor bloodDonor) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(bloodDonor) ? bloodDonor : em.merge(bloodDonor));
        em.getTransaction().commit();
        em.close();
    }
}
