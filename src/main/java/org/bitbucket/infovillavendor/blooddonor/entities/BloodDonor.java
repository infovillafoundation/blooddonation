package org.bitbucket.infovillavendor.blooddonor.entities;

import org.bitbucket.infovillavendor.blooddonor.dao.converters.GenderConverter;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodType;
import org.bitbucket.infovillavendor.blooddonor.entities.enums.Gender;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Thant Zin Oo on 10/17/2014.
 */
@NamedQueries({
        @NamedQuery(name = "BloodDonor.findAll",
                query = "SELECT e FROM BloodDonor e"),
        @NamedQuery(name = "BloodDonor.findByName",
                query = "SELECT e FROM BloodDonor e WHERE e.name = :name"),
        @NamedQuery(name = "BloodDonor.findByNameLike",
                query = "SELECT e FROM BloodDonor e WHERE e.name LIKE :name"),
        @NamedQuery(name = "BloodDonor.findByNameLikeAndBloodType",
                query = "SELECT e FROM BloodDonor e WHERE e.name LIKE :name AND e.bloodType.bloodType = :bloodType"),
})
@Entity
@Table(name = "donor")
public class BloodDonor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @Column(name = "myanmar_name")
    private String myanmarName;
    private int age;
    @Convert(converter = GenderConverter.class)
    private Gender gender;
    @Column(name = "created_date")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    private String email;
    private String facebook;
    private String phone;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "blood_type_id")
    private BloodType bloodType;
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "bloodDonor")
    private LastDonation lastDonation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMyanmarName() {
        return myanmarName;
    }

    public void setMyanmarName(String myanmarName) {
        this.myanmarName = myanmarName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BloodType getBloodType() {
        return bloodType;
    }

    public void setBloodType(BloodType bloodType) {
        this.bloodType = bloodType;
    }

    public LastDonation getLastDonation() {
        return lastDonation;
    }

    public void setLastDonation(LastDonation lastDonation) {
        this.lastDonation = lastDonation;
    }
}
