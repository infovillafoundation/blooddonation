package org.bitbucket.infovillavendor.blooddonor.entities.enums;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Thant Zin Oo on 10/27/2014.
 */

public enum DonationType {
    INTERNAL, EXTERNAL;
}
