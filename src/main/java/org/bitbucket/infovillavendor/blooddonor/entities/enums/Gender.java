package org.bitbucket.infovillavendor.blooddonor.entities.enums;

/**
 * Created by Thant Zin Oo on 10/21/2014.
 */
public enum Gender {
    MALE, FEMALE;
}
