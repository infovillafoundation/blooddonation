package org.bitbucket.infovillavendor.blooddonor.entitywrappers;

import org.bitbucket.infovillavendor.blooddonor.dao.BloodTypeDao;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodDonor;
import org.bitbucket.infovillavendor.blooddonor.entities.BloodType;
import org.bitbucket.infovillavendor.blooddonor.entities.enums.Gender;

/**
 * Created by Thant Zin Oo on 10/24/2014.
 */
public class BloodDonorWrapper {
    private BloodDonor bloodDonor;

    public BloodDonorWrapper() {
    }

    public BloodDonorWrapper(BloodDonor bloodDonor) {
        this.bloodDonor = bloodDonor;
    }

    public BloodDonor getBloodDonor() {
        return bloodDonor;
    }

    public void setBloodDonor(BloodDonor bloodDonor) {
        this.bloodDonor = bloodDonor;
    }

    public long getId() {
        return bloodDonor.getId();
    }

    public void setId(long id) {
        bloodDonor.setId(id);
    }

    public String getName() {
        return bloodDonor.getName();
    }

    public void setName(String name) {
        bloodDonor.setName(name);
    }

    public String getMyanmarName() {
        return bloodDonor.getMyanmarName();
    }

    public void setMyanmarName(String myanmarName) {
        bloodDonor.setMyanmarName(myanmarName);
    }

    public String getAge() {
        return Integer.toString(bloodDonor.getAge());
    }

    public void setAge(String age) {
        bloodDonor.setAge(Integer.parseInt(age));
    }
    public String getPhone() {
       return bloodDonor.getPhone();
    }

    public void setPhone(String phone) {
       bloodDonor.setPhone(phone);
    }


    public String getGender() {
        return bloodDonor.getGender() == Gender.MALE ? "Male" : "Female";
    }

    public void setGender(String gender) {
        if (gender.equals("Male"))
            bloodDonor.setGender(Gender.MALE);
        else
            bloodDonor.setGender(Gender.FEMALE);
    }

    public String getBloodType() {
        return bloodDonor.getBloodType().getBloodType();
    }

    public void setBloodType(String bloodType) {
        for (BloodType bt : BloodTypeDao.getBloodTypes()) {
            if (bloodType.equals(bt.getBloodType()))
                bloodDonor.setBloodType(bt);
        }
    }

    public String getEmail() {
        return bloodDonor.getEmail();
    }

    public void setEmail(String email) {
        bloodDonor.setEmail(email);
    }

    public String getFacebook() {
        return bloodDonor.getFacebook();
    }

    public void setFacebook(String facebook) {

        bloodDonor.setFacebook(facebook);
    }
}
