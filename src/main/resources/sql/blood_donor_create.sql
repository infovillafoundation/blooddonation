CREATE TABLE IF NOT EXISTS blood_type (id integer NOT NULL PRIMARY KEY, blood_type char(3) NOT NULL)

CREATE TABLE IF NOT EXISTS donor (id bigint NOT NULL PRIMARY KEY auto_increment, name varchar(50) NOT NULL, myanmar_name varchar(100) NOT NULL, age integer NOT NULL, gender char(1) NOT NULL, created_date date NOT NULL, email varchar(50), facebook varchar(50), phone varchar(15), blood_type_id integer NOT NULL)

ALTER TABLE donor ADD FOREIGN KEY (blood_type_id) REFERENCES blood_type(id)

CREATE TABLE IF NOT EXISTS last_donation (id bigint NOT NULL PRIMARY KEY auto_increment, donation_type char(1) NOT NULL, last_donation_date date NOT NULL, donor_id bigint NOT NULL)

ALTER TABLE last_donation ADD FOREIGN KEY (donor_id) REFERENCES donor(id)

CREATE TABLE IF NOT EXISTS donation (id bigint NOT NULL PRIMARY KEY auto_increment, donation_date date NOT NULL, location varchar(150) NOT NULL, donor_id bigint NOT NULL)

ALTER TABLE donation ADD FOREIGN KEY (donor_id) REFERENCES donor(id)